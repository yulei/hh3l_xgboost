import os.path
from pathlib import Path
import yaml
import uproot as up


def check_tree_exist(file_path: str, tree_name: str):
    if not os.path.exists(file_path):
        print(f'File not found: {file_path}')
        return False

    try:
        with up.open(file_path) as f:
            if tree_name not in f:
                print(f'Tree "{tree_name}" not found in file "{file_path}"')
                return False
    except Exception as e:
        return False

    return True


class Control:
    """
    Control class to record all configurations
    """
    _conf = {
        'data_dir': '',
        'data_out_dir': '',
        'model_dir': './',
        'root_suffix': 'selected.root',
        'root_prefix': 'output',
        'tree_name': 'total',
        'groups': [],
        'labels': [],
        'weight_expr': 'weight',
        'numFold': 1,
        'spectator': '',
        'cuts': '',
        'variables': [],
        'best_eta': [1],
        'best_max_depth': [1],
        'best_num_boost_round': [1],
        'auto_bin': {},
        'hyper_opt_round': 10,
        'save_to_root': False,
    }

    __setters = [
        'reweight_factor'
    ]

    @staticmethod
    def get(name):
        return Control._conf[name]

    @staticmethod
    def set(name, value):
        if name in Control.__setters:
            Control._conf[name] = value
        else:
            raise NameError("Name not accepted in set() method")

    @staticmethod
    def load(path: str):
        """
        :param path: path to config yaml
        :return:
        """
        with open(path) as file:
            config = yaml.load(file, Loader=yaml.FullLoader)

            for key in Control._conf.keys():

                if key in config.keys():
                    Control._conf[key] = config[key]
                print(f'--> {key}: {Control._conf[key]}')

        for key, value in Control._conf['groups'].items():
            Control._conf['labels'].append(key)
            Control._conf['groups'][key] = [
                f"{Control._conf['data_dir']}/"
                f"{Control._conf['root_prefix']}_{s}_{Control._conf['root_suffix']}:{Control._conf['tree_name']}"
                for s in value
            ]

            Control._conf['groups'][key] = [
                file for file in Control._conf['groups'][key] if
                check_tree_exist(file.split(':')[0], Control._conf['tree_name'])
            ]

        Control._conf['plot_dir'] = f'{Control._conf["model_dir"]}/plots/'

        for key, value in Control._conf['auto_bin'].items():
            Control._conf['auto_bin'][key] = value
