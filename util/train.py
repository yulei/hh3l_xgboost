import os

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split
import xgboost as xgb
from xgboost import XGBClassifier

from util.control import Control
from util.root_io import ReadRoot
from util.transform_binning import binned_sig
from util.plotting import plot_auc, compare_train_test, get_roc_value, plot_hyperparameters


def pred(model: XGBClassifier, data):
    return model.predict_proba(data)[:, 1]


def reweight(weights, labels, target_weights, target_labels, label_value):
    """
    Reweights the target_weights so that the sum of weights for a specific label matches the original distribution.

    Parameters:
    - weights: The original full dataset weights.
    - labels: The original full dataset labels.
    - target_weights: The weights of the target subset (train or test).
    - target_labels: The labels of the target subset (train or test).
    - label_value: The label value to apply the reweighting for (e.g., 1 for signal, 0 for background).
    """
    scaling_factor = sum(weights[labels == label_value]) / sum(target_weights[target_labels == label_value])
    target_weights[target_labels == label_value] *= scaling_factor


def data_process(n_fold: int):
    sig = ReadRoot(Control.get('groups')['signal'], label=1)
    bkg = ReadRoot(Control.get('groups')['background'], label=0)
    data_s, label_s, weights_s = sig.load_events(n_fold, train=True)
    data_b, label_b, weights_b = bkg.load_events(n_fold, train=True)

    print('==> Reweighting...')
    print('--> Original signal yields: %5.3f' % (weights_s.sum()))
    print('--> Original background yields: %5.3f' % (weights_b.sum()))
    reweight_factor = sum(weights_b) / sum(weights_s)
    weights_s = weights_s * reweight_factor
    print('--> Reweighted signal yields: %5.3f' % (weights_s.sum()))
    print('--> Reweighted background yields: %5.3f' % (weights_b.sum()))

    Control.set('reweight_factor', reweight_factor)

    data = np.concatenate((data_s, data_b), axis=0)
    label = np.concatenate((label_s, label_b), axis=0)
    weights = np.concatenate((weights_s, weights_b), axis=0)

    # set negative weight event to 0
    weights[weights < 0] = 0

    # split data into train & test
    train_data, test_data, train_label, test_label, train_weights, test_weights \
        = train_test_split(data, label, weights, test_size=0.4, shuffle=True, random_state=1)

    # Apply reweighting
    reweight(weights, label, train_weights, train_label, label_value=1)
    reweight(weights, label, test_weights, test_label, label_value=1)
    reweight(weights, label, train_weights, train_label, label_value=0)
    reweight(weights, label, test_weights, test_label, label_value=0)

    # load in data
    dtrain = xgb.DMatrix(
        data=train_data, label=train_label, weight=train_weights,
        feature_names=Control.get("variables")
    )
    dtest = xgb.DMatrix(
        data=test_data, label=test_label, weight=test_weights,
        feature_names=Control.get("variables")
    )

    del data, label, weights
    del data_s, label_s, weights_s
    del data_b, label_b, weights_b

    return dtrain, dtest, train_data, test_data, train_label, test_label, train_weights, test_weights


def train(n_fold: int):
    print(f'==> Train excluding fold {n_fold}...')
    plot_dir = f"{Control.get('plot_dir')}/fold_{n_fold}"
    os.makedirs(plot_dir, exist_ok=True)

    _, _, train_data, test_data, train_label, test_label, train_weights, test_weights = data_process(n_fold)
    # train_data = pd.DataFrame(train_data, columns=Control.get("variables"))
    # test_data = pd.DataFrame(test_data, columns=Control.get("variables"))

    # sanity check
    # assert len(Control.get('best_max_depth')) == Control.get('numFold')
    # assert len(Control.get('best_eta')) == Control.get('numFold')

    train_params = {
        # "max_depth": int(Control.get('best_max_depth')[n_fold]),
        # "learning_rate": Control.get('best_eta')[n_fold],  # 'eta' is 'learning_rate' in XGBClassifier
        # "n_estimators": Control.get('best_num_boost_round')[n_fold],  # 'num_boost_round' is 'n_estimators'
        "max_depth": int(Control.get('best_max_depth')),
        "learning_rate": Control.get('best_eta'),  # 'eta' is 'learning_rate' in XGBClassifier
        "n_estimators": Control.get('best_num_boost_round'),  # 'num_boost_round' is 'n_estimators'
        "objective": "binary:logistic",
        "eval_metric": ["logloss", "auc"],  # This works directly in fit
        "early_stopping_rounds": 10,
        # Uncomment if using GPU
        # "tree_method": "gpu_hist"
    }

    task_params = {
        "eval_set": [(train_data, train_label), (test_data, test_label)],
        "sample_weight_eval_set": [train_weights, test_weights],
        "verbose": True,
    }

    xgb_classifier = XGBClassifier(**train_params)
    xgb_classifier.fit(
        X=train_data,
        y=train_label,
        sample_weight=train_weights,
        **task_params
    )

    # evaluation on test
    preds = xgb_classifier.predict_proba(test_data)[:, 1]

    # draw plots
    fig, ax = plt.subplots(figsize=(6, 10))
    xgb.plot_importance(
        xgb_classifier, ax=ax, importance_type='gain', show_values=False,
    )
    feature_map = {f'f{i}': feature_name for i, feature_name in enumerate(Control.get('variables'))}
    ax.set_yticklabels([feature_map[label.get_text()] for label in ax.get_yticklabels()])
    plt.savefig(f'{plot_dir}/importance_{n_fold}.pdf')
    plt.savefig(f'{plot_dir}/importance_{n_fold}.png')
    plt.close()

    # print(
    #     "best iteration = %d, error = %f, weighted_error = %f"
    #     % (bst.best_iteration,
    #        sum(1 for i in range(len(preds)) if int(preds[i] > 0.5) != test_label[i])
    #        / float(len(preds)),
    #        sum(weights[i] for i in range(len(preds)) if int(preds[i] > 0.5) != test_label[i])
    #        / test_weights.sum()
    #        )
    # )

    plot_auc(test_label, preds, test_weights, save_path=f'{plot_dir}/auc_roc_{n_fold}.pdf')
    compare_train_test(
        pred(xgb_classifier, train_data),
        np.transpose(train_label)[0], preds, np.transpose(test_label)[0],
        xlabel="XGboost score", title="XGboost",
        weights_train=np.transpose(train_weights)[0], weights_test=np.transpose(test_weights)[0],
        save_path=f'{plot_dir}/train_test_{n_fold}.pdf',
        auto_bining_cfg=Control.get('auto_bin'),
        bins='auto',
    )

    # save model
    os.makedirs(Control.get('model_dir'), exist_ok=True)
    xgb_classifier.save_model(f'{Control.get("model_dir")}/xgb_{n_fold}.json')

    if Control.get('save_to_root'):
        import ROOT
        ROOT.TMVA.Experimental.SaveXGBoost(
            xgb_classifier, 'xgb', f'{Control.get("model_dir")}/xgb_{n_fold}.root', num_inputs=train_data.shape[1]
        )

    # clean memory
    del train_data, test_data, train_label, test_label, train_weights, test_weights
    del preds,

    # return the model
    return xgb_classifier


def hyper_train(args):
    from hyperopt import STATUS_OK

    # Hyperopt configuration for XGBClassifier
    train_params = {
        # "tree_method": "gpu_hist",  # Uncomment if using GPU
        "max_depth": int(args['max_depth']),
        "learning_rate": float(args['eta']),  # 'eta' is 'learning_rate' in XGBClassifier
        "objective": "binary:logistic",
        "eval_metric": ["logloss"],  # Specify evaluation metric
        "n_estimators": int(args['num_boost_round']),  # Replace 'num_boost_round' with 'n_estimators'
        "early_stopping_rounds": 10
    }

    task_params = {
        "eval_set": [
            (args['train'].get_data(), args['train'].get_label()),
            (args['test'].get_data(), args['test'].get_label())
        ],
        "sample_weight_eval_set": [args['train'].get_weight(), args['test'].get_weight()],
        "verbose": False
    }

    # Initialize XGBClassifier with train_params
    xgb_classifier = XGBClassifier(**train_params)

    # Fit the classifier using task_params
    xgb_classifier.fit(
        X=args['train'].get_data(),
        y=args['train'].get_label(),
        sample_weight=args['train'].get_weight(),
        **task_params
    )

    # Evaluation on test data
    preds = xgb_classifier.predict_proba(args['test'].get_data())[:, 1]
    _, significance = binned_sig(
        preds, args['test_label'], args['test_weights'],
        reweight_factor=Control.get('reweight_factor'), **Control.get('auto_bin')
    )

    return {'loss': -significance, 'status': STATUS_OK}


def hyper_tuning(n_fold: int):
    from hyperopt import hp, Trials, fmin, tpe

    dtrain, dtest, train_data, test_data, train_label, test_label, train_weights, test_weights = data_process(n_fold)

    # define a search space
    space = {
        'max_depth': hp.quniform("max_depth", 3, 18, 1),
        'eta': hp.uniform('eta', 0.01, 2),
        'num_boost_round': hp.quniform("num_boost_round", 50, 300, 1),
        'train': dtrain,
        'test': dtest,
        'test_label': test_label,
        'test_weights': test_weights,
    }

    # minimize the objective over the space
    trials = Trials()

    best_hyperparams = fmin(fn=hyper_train,
                            space=space,
                            algo=tpe.suggest,
                            max_evals=Control.get('hyper_opt_round'),
                            trials=trials)

    os.makedirs(Control.get('plot_dir'), exist_ok=True)
    plot_hyperparameters(
        trials, save_path=f'{Control.get("plot_dir")}/hyper_parameters_{n_fold}.pdf', eval_name='Significance'
    )

    print("The best hyper parameters are : ", "\n")
    print(best_hyperparams)
