import xgboost as xgb
from util.root_io import ReadRoot
from util.control import Control


def apply(n_fold: int, bst):
    print(f'==> Apply to fold {n_fold}...')

    for _, group in Control.get('groups').items():
        read = ReadRoot(group, label=-1)
        read.update_events(n_fold=n_fold, model=bst)
