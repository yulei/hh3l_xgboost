from sklearn import metrics
from sklearn.metrics import roc_curve, auc
import matplotlib.colors as mcolors
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import matplotlib.cm as cm
from util.transform_binning import binned_sig
from util.control import Control


# Plot score for signal and background, comparing training and testing
def compare_train_test(
        y_pred_train, y_train, y_pred, y_test, high_low=(0, 1),
        bins=30, xlabel="", ylabel="Arbitrary units", title="",
        weights_train=np.array([]), weights_test=np.array([]),
        density=True,
        save_path='train_test.pdf',
        auto_bining_cfg=None,
        reweight_back=True,
):
    if weights_train.size != 0:
        weights_train_signal = weights_train[y_train == 1]
        weights_train_background = weights_train[y_train == 0]
    else:
        weights_train_signal = None
        weights_train_background = None

    # calculate auto-binning
    if bins == 'auto':
        bins, significance = binned_sig(
            y_pred, y_test, weights_test,
            reweight_factor=Control.get('reweight_factor') if reweight_back else 1,
            **auto_bining_cfg
        )
        bins = np.array(bins)
        print(f'==> [Test] Auto-binning: {bins}')
        print(f'==> [Test] Significance: {significance}')

    plt.figure(figsize=(10, 10))
    plt.hist(
        y_pred_train[y_train == 1],
        color='r', alpha=0.5, range=high_low, bins=bins,
        histtype='stepfilled', density=density,
        label='S (train)', weights=weights_train_signal
    )  # alpha is transparency
    plt.hist(
        y_pred_train[y_train == 0],
        color='b', alpha=0.5, range=high_low, bins=bins,
        histtype='stepfilled', density=density,
        label='B (train)', weights=weights_train_background
    )

    if weights_test.size != 0:
        weights_test_signal = weights_test[y_test == 1]
        weights_test_background = weights_test[y_test == 0]
    else:
        weights_test_signal = None
        weights_test_background = None
    hist, bins = np.histogram(
        y_pred[y_test == 1], bins=bins, range=high_low, density=density, weights=weights_test_signal
    )
    scale = len(y_pred[y_test == 1]) / sum(hist)
    err = np.sqrt(hist * scale) / scale

    center = (bins[:-1] + bins[1:]) / 2
    plt.errorbar(center, hist, yerr=err, fmt='o', c='r', label='S (test)')

    hist, bins = np.histogram(
        y_pred[y_test == 0], bins=bins, range=high_low, density=density, weights=weights_test_background
    )
    scale = len(y_pred[y_test == 0]) / sum(hist)
    err = np.sqrt(hist * scale) / scale

    center = (bins[:-1] + bins[1:]) / 2
    plt.errorbar(center, hist, yerr=err, fmt='o', c='b', label='B (test)')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend(loc='best')
    plt.grid()
    # plt.yscale("log")
    plt.savefig(save_path)
    plt.savefig(save_path.replace('.pdf', '.png'))
    plt.close()


def get_roc_value(y_true, y_score, sample_weights):
    fpr, tpr, thresholds = metrics.roc_curve(y_true, y_score, sample_weight=sample_weights)
    return auc(fpr, tpr)


def plot_auc(y_true, y_score, sample_weights, save_path='auc_roc.pdf'):
    # AUC Curve
    fpr, tpr, thresholds = metrics.roc_curve(y_true, y_score, sample_weight=sample_weights)
    roc_auc = auc(fpr, tpr)

    plt.figure()
    lw = 2
    plt.plot(
        fpr,
        tpr,
        color="darkorange",
        lw=lw,
        label="ROC curve (area = %0.2f)" % roc_auc,
    )
    plt.plot([0, 1], [0, 1], color="navy", lw=lw, linestyle="--")
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("Receiver operating characteristic example")
    plt.legend(loc="lower right")
    plt.savefig(save_path)
    plt.savefig(save_path.replace('.pdf', '.png'))
    # plt.show()

    plt.close()


def plot_hyperparameters(trials, eval_name='AUC', save_path='hyper_parameters.pdf'):
    # Initialize an empty list to hold trial results
    results = []

    for trial in trials.trials:
        result = trial['result']
        params = {key: np.array(val).item() for key, val in trial['misc']['vals'].items()}
        params[eval_name] = -result['loss']
        results.append(params)

    # Convert to DataFrame
    df_results = pd.DataFrame(results)

    # Step 2: Normalize the data
    scaler = MinMaxScaler()
    df_normalized = pd.DataFrame(scaler.fit_transform(df_results), columns=df_results.columns)

    # Identify the trial with the best (lowest) loss
    best_trial_index = df_results[eval_name].idxmax()
    best_trial_params = df_results.loc[best_trial_index]
    df_normalized['Trial'] = range(len(df_normalized))

    # Create color mapping based on loss values
    norm = plt.Normalize(df_results[eval_name].min(), df_results[eval_name].max())
    colors = cm.rainbow(norm(df_results[eval_name]))

    # Plot all trials with normal width
    fig, ax = plt.subplots(figsize=(12, 6))  # Create a figure and axis for the plot
    for i in range(len(df_normalized)):
        linewidth = 2 if i == best_trial_index else 1  # Thicker line for the best trial
        linestyle = '-' if i == best_trial_index else '--'
        marker = 'o' if i == best_trial_index else None
        ax.plot(
            df_normalized.columns[:-1], df_normalized.iloc[i, :-1], color=colors[i], alpha=0.7,
            linewidth=linewidth, linestyle=linestyle, marker=marker,
        )

    for param in best_trial_params.index[:-1]:  # Skip the 'Trial' column
        x_pos = df_normalized.columns.get_loc(param)
        y_pos = df_normalized.iloc[best_trial_index, x_pos]
        ax.text(
            x_pos, y_pos + 0.05, f"{best_trial_params[param]:.3f}",
            ha='center', va='bottom', fontsize=12, color='blue'
        )

    # Customize the plot
    ax.set_yticks([])
    ax.grid()

    # Create the color bar with a ScalarMappable
    norm = mcolors.Normalize(vmin=df_normalized[eval_name].min(), vmax=df_normalized[eval_name].max())
    sm = cm.ScalarMappable(norm=norm, cmap='rainbow')
    cbar = fig.colorbar(sm, ax=ax, label=eval_name)  # Explicitly associate the color bar with the figure

    plt.tight_layout()

    # Save the plot
    plt.savefig(save_path)
    plt.savefig(save_path.replace('.pdf', '.png'))
    # plt.show()
    plt.close()
