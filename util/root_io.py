import os

import numpy as np
import uproot as up
from util.control import Control
import xgboost as xgb


class ReadRoot:
    def __init__(self, path_list, label):
        super().__init__()
        self.path_list = path_list
        self.label = label
        self.cv = ''

    def cv_selection(self, n_fold: int, train=True):
        self.cv = f'(({Control.get("spectator")} % {Control.get("numFold")} != {n_fold})' \
                  f' & ({Control.get("cuts")}))' \
            if train \
            else \
            f'(({Control.get("spectator")} % {Control.get("numFold")} == {n_fold}))'

    def load_events(self, n_fold: int, train=True):
        self.cv_selection(n_fold, train=train)

        weights, label, data = None, None, None
        for batch, report in up.iterate(self.path_list, library='np',
                                        expressions=[Control.get("weight_expr"), Control.get("spectator")]
                                                    + Control.get("variables"),
                                        cut=self.cv,
                                        report=True,
                                        ):
            print(f'--> Reading {report.stop - report.start} events from {report.file_path}')

            _weights = batch[Control.get("weight_expr")]
            length = len(_weights)
            _weights = _weights.reshape(length, 1)
            _label = (np.ones(length) * self.label).reshape(length, 1)
            _data = np.concatenate([batch[v].reshape(length, 1) for v in Control.get("variables")], axis=1)

            weights = _weights if weights is None else np.concatenate((_weights, weights), axis=0)
            label = _label if label is None else np.concatenate((_label, label), axis=0)
            data = _data if data is None else np.concatenate((_data, data), axis=0)

            print(f'--> Reading {length} events for label {self.label} satisfying {self.cv}')
        return data, label, weights

    def update_events(self, n_fold: int, model):
        self.cv_selection(n_fold, train=False)

        os.makedirs(Control.get('data_out_dir'), exist_ok=True)
        for file in self.path_list:
            tree_name = file.rsplit(':', 1)[1]
            out_file_name = Control.get('data_out_dir') + \
                            (file.split(':', 1))[0].rsplit('/', 1)[1].rsplit('.', 1)[0] + f'_{n_fold}.root'
            print(f'--> Input file: "{file}"; ==> Output file: "{out_file_name}" with tree name "{tree_name}"')

            # opening a file to write
            with up.recreate(out_file_name) as out_file:
                for batch, report in up.iterate(file, library='np',
                                                cut=self.cv,
                                                report=True,
                                                step_size="300 MB",
                                                ):
                    percentage = report.stop / report.tree.num_entries
                    print(f'--> Reading {report.start} events from {report.file_path} [%4.2f]' % percentage)

                    _data = np.concatenate([batch[v].reshape(batch[Control.get("variables")[0]].size, 1) for v in
                                            Control.get("variables")], axis=1)
                    predicts = model.predict(xgb.DMatrix(data=_data, feature_names=Control.get("variables")))

                    batch.update({'xgboost_score': predicts})
                    if report.start == 0:
                        out_file[tree_name] = batch
                    else:
                        out_file[tree_name].extend(batch)
