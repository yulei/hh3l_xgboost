from util.train import train, hyper_tuning
from util.apply import apply
from util.control import Control
import argparse
import xgboost as xgb

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train xgboost for HH -> multi-lepton.')
    # parser.add_argument('--run_type', type=int, default=0, metavar='all (0)',
    #                     help='choose all(0), apply(1), train(2), fine-tuning(3)')
    parser.add_argument('--run_type', type=str, default='all', metavar='all',
                        choices=['all', 'apply', 'train', 'hyper_opt'],
                        help='run type')
    parser.add_argument('--fold_to_apply', metavar='n', type=int, default=0, required=True,
                        help='which fold to apply (excluded in train)')
    # add config file
    parser.add_argument('--config', metavar='config.yaml', type=str, required=True, help='config file')

    args = parser.parse_args()

    Control.load(args.config)
    print(Control.get('groups'))
    print(Control.get('labels'))

    if args.run_type == 'all':
        model = train(args.fold_to_apply)
        apply(args.fold_to_apply, model)
    if args.run_type == 'apply':
        model = xgb.Booster()
        model.load_model(f'{Control.get("model_dir")}_{args.fold_to_apply}.json')
        apply(args.fold_to_apply, model)
    if args.run_type == 'train':
        train(args.fold_to_apply)
    if args.run_type == 'hyper_opt':
        hyper_tuning(args.fold_to_apply)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
