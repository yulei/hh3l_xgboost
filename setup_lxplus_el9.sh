#!/bin/bash

# lxplus
source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc11-opt/setup.sh

# add current file path to PYTHONPATH and path
current_path=$(dirname "$(realpath ${BASH_SOURCE})")
echo "Add path ${current_path} to PYTHONPATH"
export PYTHONPATH=$current_path:$PYTHONPATH
export PATH=$current_path:$PATH


# Define the virtual environment path
env_name="hhml_xgb"
venv_path="${current_path}/${env_name}"

# Check if the virtual environment already exists
if [ ! -d "$venv_path" ]; then
    echo "Creating a new virtual environment at ${venv_path}"
    python -m venv "$venv_path"
    source "${venv_path}/bin/activate"

    # Install the missing package
    echo "Installing the missing package in the virtual environment"
    pip install hyperopt
else
    echo "Activating the existing virtual environment at ${venv_path}"
    source "${venv_path}/bin/activate"

    # Check if the missing package is already installed
    if ! pip show hyperopt > /dev/null 2>&1; then
        echo "Installing the missing package"
        pip install hyperopt
    else
        echo "The missing package is already installed"
    fi
fi

# Inform the user
echo "Environment setup complete. Using LCG packages and additional virtual environment packages."